package com.cpip.common.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ForgetQo {
    private String account;
    private String password;
    private String contact;
}
