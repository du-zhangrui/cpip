package com.cpip.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Goods {
    @TableId(type = IdType.AUTO)
    private Long goodsId;
    private String goodsName;
    private int goodsNum;
    private int price;
    private String description;
    private Long manufacturerId;
    private String manufacturerName;
    private String manufacturerAddress;
}
