package com.cpip.common.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    @TableId(type = IdType.AUTO)
    private Long cartId;
    private Long goodsId;
    private String goodsName;
    private int goodsNum;
    private int goodsPrice;
    private int totalPrice;
    private String status;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    private Long manufacturerId;
    private String manufacturerName;
    private String manufacturerAddress;
    private String manufacturerContact;
    private Long buyerId;
    private String buyerName;
    private String buyerAddress;
    private String buyerContact;
    //逻辑删除
    @TableLogic
    private int isDeleted;
}

