package com.cpip.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Buyer {
    @TableId(type = IdType.AUTO)
    private Long buyerId;
    private String account;
    private String buyerName;
    private String buyerPassword;
    private String buyerAddress;
    private String contact;
    private int money;
    private boolean buyerState;
}
