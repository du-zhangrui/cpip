package com.cpip.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Manufacturer {
    @TableId(type = IdType.AUTO)
    private Long manufacturerId;
    private String account;
    private String manufacturerName;
    private String manufacturerPassword;
    private String manufacturerAddress;
    private String contact;
    private int money;
    private boolean manufacturerState;
}
