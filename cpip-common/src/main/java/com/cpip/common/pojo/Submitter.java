package com.cpip.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Submitter {
    @TableId(type = IdType.AUTO)
    private Long submitterId;
    private String account;
    private String submitterName;
    private String submitterPassword;
    private String submitterNickname;
    private String submitterIdentity;
    private String contact;
    private boolean submitterState;
}
