package com.cpip.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplyInformation {
    @TableId(type = IdType.AUTO)
    private int applyInformationId;
    private String goodsName;
    private int goodsNum;
    private String status;
    private int submitterId;
    private String submitterName;
    private String submitterIdentity;
    private String submitterContact;
    private int buyerId;
    private String buyerName;
    private String buyerContact;
}
