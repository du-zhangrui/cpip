package com.cpip.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataResponse {
    //表示状态码的数字
    private Integer status;
    //携带详细信息的字符串
    private String msg;
    //携带的各种数据
    private Object data;

    //编写静态方法,获取具有参数数据的SysResult,失败做准备
    public static DataResponse error(Object data){
        return new DataResponse(444, "error", data);
    }
    public static DataResponse success(){
        return new DataResponse(200, "success", null);
    }
    public static DataResponse success(Object data){
        return new DataResponse(200, "success", data);
    }
}
