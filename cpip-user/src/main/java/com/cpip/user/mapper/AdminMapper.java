package com.cpip.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cpip.common.pojo.Admin;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminMapper extends BaseMapper<Admin> {
}
