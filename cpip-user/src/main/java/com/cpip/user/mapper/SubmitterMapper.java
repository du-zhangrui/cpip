package com.cpip.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cpip.common.pojo.Submitter;
import org.springframework.stereotype.Repository;

@Repository
public interface SubmitterMapper extends BaseMapper<Submitter> {
}
