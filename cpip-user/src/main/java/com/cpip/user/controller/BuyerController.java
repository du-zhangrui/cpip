package com.cpip.user.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.ApplyInformation;
import com.cpip.common.pojo.Buyer;
import com.cpip.common.qo.ForgetQo;
import com.cpip.common.vo.DataResponse;
import com.cpip.user.config.JwtUtil;
import com.cpip.user.service.BuyerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@EnableAutoConfiguration
@RestController
@RequestMapping("/user/buyer")
public class BuyerController {
    @Autowired
    private BuyerService buyerService;

    @Autowired
    private JwtUtil jwtUtil;

    //根据账号查询某一个用户
    @GetMapping("/selectOne")
    public DataResponse selectOne(@RequestParam String account) {
        Buyer buyer = buyerService.selectOne(account);
        if (buyer != null) {
            return DataResponse.success(buyer);
        } else {
            return DataResponse.error("用户不存在");
        }
    }

    //查询账号是否存在
    @GetMapping("/selectAccount")
    public DataResponse selectAccount(@RequestParam String account) {
        boolean result = buyerService.selectAccount(account);
        if (result) {
            return DataResponse.success("账号已经存在");
        } else {
            return DataResponse.success("恭喜你，账号可以使用");
        }
    }

    //修改密码
    @GetMapping("/editPassword")
    public DataResponse editPassword(@RequestParam String account, @RequestParam String currentPassword, @RequestParam String newPassword) {
        boolean result = buyerService.editPassword(account, currentPassword, newPassword);
        if (result) {
            return DataResponse.success("修改成功");
        } else {
            return DataResponse.error("当前密码输入有误，请检查");
        }
    }

    //修改个人信息
    @GetMapping("/editMessage")
    public DataResponse editMessage(@RequestParam String account, @RequestParam String buyerName, @RequestParam String contact) {
        try {
            buyerService.editMessage(account, buyerName, contact);
            return DataResponse.success("修改信息成功");
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改信息失败");
        }
    }

    //根据采购员id查询采购信息
    @GetMapping("/selectApplyInformation")
    public DataResponse selectApplyInformation(@RequestParam int buyerId, @RequestParam int current, @RequestParam int pageSize, String query) {
        Page<ApplyInformation> applyInformationPage = buyerService.selectApplyInformation(buyerId, current, pageSize, query);
        if (applyInformationPage != null) {
            return DataResponse.success(applyInformationPage);
        } else {
            return DataResponse.error("查询不到采购信息");
        }
    }

    //解析token
    @GetMapping("/getInformationFromToken")
    public DataResponse getToken(@RequestParam String token) {
        String jsonInformation = jwtUtil.getInformationFromToken(token);
        Buyer buyer = JSON.parseObject(jsonInformation, Buyer.class);
        if (buyer != null) {
            return DataResponse.success(buyer);
        } else {
            return DataResponse.error("解析失败");
        }
    }

    //查询所有用户
    @GetMapping("/selectAll")
    public DataResponse selectAll(@RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Buyer> buyerPage = buyerService.selectAll(current, pageSize, query);
        if (buyerPage != null) {
            return DataResponse.success(buyerPage);
        } else {
            return DataResponse.error("查询不到用户");
        }
    }

    //注册
    @PostMapping("/save")
    public DataResponse regist(@RequestBody Buyer buyer) {
        boolean result = buyerService.selectAccount(buyer.getAccount());
        if (result) {
            return DataResponse.error("账号已经存在");
        } else {
            try {
                buyerService.regist(buyer);
                return DataResponse.success();
            } catch (Exception e) {
                e.printStackTrace();
                return DataResponse.error("注册失败");
            }
        }
    }

    //登录校验
    @PostMapping("/login")
    public DataResponse login(@RequestBody Buyer buyer) {
        Buyer buyerLogin = buyerService.login(buyer.getAccount(), buyer.getBuyerPassword());
        if (buyerLogin == null) {
            return DataResponse.error("登录失败");
        }
        //生成令牌
        String jsonInformation = JSON.toJSONString(buyerLogin);
        String token = jwtUtil.generateToken(jsonInformation);
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("account", buyerLogin.getAccount());
        return DataResponse.success(map);
    }

    //修改
    @PutMapping("/update")
    public DataResponse update(@RequestBody Buyer buyer) {
        try {
            buyerService.update(buyer);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //完善采购信息
    @GetMapping("/updateApplyInformation")
    public DataResponse saveApplyInformation(@RequestParam int buyerId, @RequestParam int applyInformationId) {
        boolean result = buyerService.updateApplyInformation(buyerId, applyInformationId);
        if (result){
            return DataResponse.success("更新成功");
        }else {
            return DataResponse.error("请完善信息");
        }
    }

    //金额充值
    @GetMapping("/addMoney")
    public DataResponse addMoney(@RequestParam int buyerId, @RequestParam int money) {
        try {
            buyerService.addMoney(buyerId, money);
            return DataResponse.success("充值成功");
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.success("充值失败");
        }
    }

    //忘记密码
    @PutMapping("/forgetPassword")
    public DataResponse forgetPassword(@RequestBody ForgetQo forgetQo) {
        try {
            boolean result = buyerService.forgetPassword(forgetQo);
            if (result) {
                return DataResponse.success();
            } else {
                return DataResponse.error("手机号验证错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //根据账号修改状态
    @PutMapping("/{account}/state/{state}")
    public DataResponse updateState(@PathVariable String account, @PathVariable boolean state) {
        try {
            buyerService.updateState(account, state);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //删除
    @DeleteMapping("/delete")
    public DataResponse delete(@RequestParam String account) {
        int result = buyerService.delete(account);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("删除失败");
        }
    }
}
