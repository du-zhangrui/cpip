package com.cpip.user.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Manufacturer;
import com.cpip.common.qo.ForgetQo;
import com.cpip.common.vo.DataResponse;
import com.cpip.user.config.JwtUtil;
import com.cpip.user.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user/manufacturer")
public class ManufacturerController {
    @Autowired
    private ManufacturerService manufacturerService;
    @Autowired
    private JwtUtil jwtUtil;

    //根据账号查询某一个用户
    @GetMapping("/selectOne")
    public DataResponse selectOne(@RequestParam String account) {
        Manufacturer manufacturer = manufacturerService.selectOne(account);
        if (manufacturer != null) {
            return DataResponse.success(manufacturer);
        } else {
            return DataResponse.error("用户不存在");
        }
    }

    //修改密码
    @GetMapping("/editPassword")
    public DataResponse editPassword(@RequestParam String account, @RequestParam String currentPassword, @RequestParam String newPassword) {
        boolean result = manufacturerService.editPassword(account, currentPassword, newPassword);
        if (result) {
            return DataResponse.success("修改成功");
        } else {
            return DataResponse.error("当前密码输入有误，请检查");
        }
    }

    //修改个人信息
    @GetMapping("/editMessage")
    public DataResponse editMessage(@RequestParam String account, @RequestParam String manufacturerAddress, @RequestParam String contact) {
        try {
            manufacturerService.editMessage(account, manufacturerAddress, contact);
            return DataResponse.success("修改信息成功");
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改信息失败");
        }
    }

    //查询账号是否存在
    @GetMapping("/selectAccount")
    public DataResponse selectAccount(@RequestParam String account) {
        boolean result = manufacturerService.selectAccount(account);
        if (result) {
            return DataResponse.success("账号已经存在");
        } else {
            return DataResponse.success("恭喜你，账号可以使用");
        }
    }

    //解析token
    @GetMapping("/getInformationFromToken")
    public DataResponse getToken(@RequestParam String token) {
        String jsonInformation = jwtUtil.getInformationFromToken(token);
        Manufacturer manufacturer = JSON.parseObject(jsonInformation, Manufacturer.class);
        if (manufacturer != null) {
            return DataResponse.success(manufacturer);
        } else {
            return DataResponse.error("解析失败");
        }
    }

    //查询所有厂家
    @GetMapping("/selectAll")
    public DataResponse selectAll(@RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Manufacturer> manufacturerPage = manufacturerService.selectAll(current, pageSize, query);
        if (manufacturerPage != null) {
            return DataResponse.success(manufacturerPage);
        } else {
            return DataResponse.error("查询不到用户");
        }
    }

    //登录校验
    @PostMapping("/login")
    public DataResponse login(@RequestBody Manufacturer manufacturer) {
        Manufacturer manufacturerLogin = manufacturerService.login(manufacturer.getAccount(), manufacturer.getManufacturerPassword());
        if (manufacturerLogin == null) {
            return DataResponse.error("登录失败");
        }
        //生成令牌
        String jsonInformation = JSON.toJSONString(manufacturerLogin);
        String token = jwtUtil.generateToken(jsonInformation);
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("account", manufacturerLogin.getAccount());
        return DataResponse.success(map);
    }

    //注册厂家
    @PostMapping("/save")
    public DataResponse regist(@RequestBody Manufacturer manufacturer) {
        boolean result = manufacturerService.selectAccount(manufacturer.getAccount());
        if (result) {
            return DataResponse.error("账号已经存在");
        } else {
            try {
                manufacturerService.regist(manufacturer);
                return DataResponse.success();
            } catch (Exception e) {
                e.printStackTrace();
                return DataResponse.error("注册失败");
            }
        }
    }

    //忘记密码
    @PutMapping("/forgetPassword")
    public DataResponse forgetPassword(@RequestBody ForgetQo forgetQo) {
        try {
            boolean result = manufacturerService.forgetPassword(forgetQo);
            if (result) {
                return DataResponse.success();
            } else {
                return DataResponse.error("手机号验证错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //修改
    @PutMapping("/update")
    public DataResponse update(@RequestBody Manufacturer manufacturer) {
        try {
            manufacturerService.update(manufacturer);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //根据账号修改状态
    @PutMapping("/{account}/state/{state}")
    public DataResponse updateState(@PathVariable String account, @PathVariable boolean state) {
        try {
            manufacturerService.updateState(account, state);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //删除
    @DeleteMapping("/delete")
    public DataResponse delete(@RequestParam String account) {
        int result = manufacturerService.delete(account);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("删除失败");
        }
    }
}
