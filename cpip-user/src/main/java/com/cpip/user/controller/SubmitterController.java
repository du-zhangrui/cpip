package com.cpip.user.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.ApplyInformation;
import com.cpip.common.pojo.Submitter;
import com.cpip.common.qo.ForgetQo;
import com.cpip.common.vo.DataResponse;
import com.cpip.user.config.JwtUtil;
import com.cpip.user.service.SubmitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user/submitter")
public class SubmitterController {
    @Autowired
    private SubmitterService submitterService;
    @Autowired
    private JwtUtil jwtUtil;

    //根据账号查询某一个用户
    @GetMapping("/selectOne")
    public DataResponse selectOne(@RequestParam String account) {
        Submitter submitter = submitterService.selectOne(account);
        if (submitter != null) {
            return DataResponse.success(submitter);
        } else {
            return DataResponse.error("用户不存在");
        }
    }

    //查询账号是否存在
    @GetMapping("/selectAccount")
    public DataResponse selectAccount(@RequestParam String account) {
        boolean result = submitterService.selectAccount(account);
        if (result) {
            return DataResponse.success("账号已经存在");
        } else {
            return DataResponse.success("恭喜你，账号可以使用");
        }
    }

    //解析token
    @GetMapping("/getInformationFromToken")
    public DataResponse getToken(@RequestParam String token) {
        String jsonInformation = jwtUtil.getInformationFromToken(token);
        Submitter submitter = JSON.parseObject(jsonInformation, Submitter.class);
        if (submitter != null) {
            return DataResponse.success(submitter);
        } else {
            return DataResponse.error("解析失败");
        }
    }

    //查询上报员
    @GetMapping("/selectAll")
    public DataResponse selectAll(@RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Submitter> submitterPage = submitterService.selectAll(current, pageSize, query);
        if (submitterPage != null) {
            return DataResponse.success(submitterPage);
        } else {
            return DataResponse.error("查询不到用户");
        }
    }

    //根据上报员id查询采购信息
    @GetMapping("/selectApplyInformation")
    public DataResponse selectApplyInformation(@RequestParam int submitterId, @RequestParam int current, @RequestParam int pageSize, String query) {
        Page<ApplyInformation> applyInformationPage = submitterService.selectApplyInformation(submitterId, current, pageSize, query);
        if (applyInformationPage != null) {
            return DataResponse.success(applyInformationPage);
        } else {
            return DataResponse.error("查询不到采购信息");
        }
    }

    //登录校验
    @PostMapping("/login")
    public DataResponse login(@RequestBody Submitter submitter) {
        Submitter submitterLogin = submitterService.login(submitter.getAccount(), submitter.getSubmitterPassword());
        if (submitterLogin == null) {
            return DataResponse.error("登录失败");
        }
        //生成令牌
        String jsonInformation = JSON.toJSONString(submitterLogin);
        String token = jwtUtil.generateToken(jsonInformation);
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("account", submitterLogin.getAccount());
        return DataResponse.success(map);
    }

    //根据id查询采购信息
    @GetMapping("/selectApplyInformationById")
    public DataResponse selectApplyInformationById(@RequestParam int applyInformationId) {
        ApplyInformation applyInformation = submitterService.selectApplyInformationById(applyInformationId);
        if (applyInformation != null) {
            return DataResponse.success(applyInformation);
        } else {
            return DataResponse.error("查询不到采购信息");
        }
    }

    //新增采购信息
    @PostMapping("/saveApplyInformation")
    public DataResponse saveApplyInformation(@RequestBody ApplyInformation applyInformation) {
        boolean result = submitterService.saveApplyInformation(applyInformation);
        if (result) {
            return DataResponse.success();
        } else {
            return DataResponse.error("请完善个人联系方式！");
        }
    }

    //修改密码
    @GetMapping("/editPassword")
    public DataResponse editPassword(@RequestParam String account, @RequestParam String currentPassword, @RequestParam String newPassword) {
        boolean result = submitterService.editPassword(account, currentPassword, newPassword);
        if (result) {
            return DataResponse.success("修改成功");
        } else {
            return DataResponse.error("当前密码输入有误，请检查");
        }
    }

    //修改个人信息
    @GetMapping("/editMessage")
    public DataResponse editMessage(@RequestParam String account, String submitterNickname, @RequestParam String contact) {
        try {
            submitterService.editMessage(account, submitterNickname, contact);
            return DataResponse.success("修改信息成功");
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改信息失败");
        }
    }

    //修改采购信息
    @PutMapping("/updateApplyInformation")
    public DataResponse updateApplyInformation(@RequestBody ApplyInformation applyInformation) {
        boolean result = submitterService.updateApplyInformation(applyInformation);
        if (result) {
            return DataResponse.success("修改成功");
        } else {
            return DataResponse.error("修改失败");
        }
    }

    //删除采购信息
    @DeleteMapping("/deleteApplyInformation")
    public DataResponse deleteApplyInformation(@RequestParam int applyInformationId) {
        int result = submitterService.deleteApplyInformation(applyInformationId);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("删除失败");
        }
    }

    //注册
    @PostMapping("/save")
    public DataResponse regist(@RequestBody Submitter submitter) {
        boolean result = submitterService.selectAccount(submitter.getAccount());
        if (result) {
            return DataResponse.error("账号已经存在");
        } else {
            try {
                submitterService.regist(submitter);
                return DataResponse.success();
            } catch (Exception e) {
                e.printStackTrace();
                return DataResponse.error("注册失败");
            }
        }
    }

    //忘记密码
    @PutMapping("/forgetPassword")
    public DataResponse forgetPassword(@RequestBody ForgetQo forgetQo) {
        try {
            boolean result = submitterService.forgetPassword(forgetQo);
            if (result) {
                return DataResponse.success();
            } else {
                return DataResponse.error("手机号验证错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //修改
    @PutMapping("/update")
    public DataResponse update(@RequestBody Submitter submitter) {
        try {
            submitterService.update(submitter);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //根据账号修改状态
    @PutMapping("/{account}/state/{state}")
    public DataResponse updateState(@PathVariable String account, @PathVariable boolean state) {
        try {
            submitterService.updateState(account, state);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //删除
    @DeleteMapping("/delete")
    public DataResponse delete(@RequestParam String account) {
        int result = submitterService.delete(account);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("删除失败");
        }
    }
}
