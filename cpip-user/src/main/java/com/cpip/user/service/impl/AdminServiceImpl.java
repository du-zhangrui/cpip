package com.cpip.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cpip.common.pojo.Admin;
import com.cpip.user.mapper.AdminMapper;
import com.cpip.user.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public void update(Admin admin) {
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();
        wrapper.eq("account", admin.getAccount());
        adminMapper.update(admin, wrapper);
    }

    @Override
    public Admin login(String account, String adminPassword) {
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        wrapper.eq("admin_password", adminPassword);
        Admin admin = adminMapper.selectOne(wrapper);
        return admin;

    }
}
