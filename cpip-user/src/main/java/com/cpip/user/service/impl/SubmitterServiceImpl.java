package com.cpip.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.ApplyInformation;
import com.cpip.common.pojo.Submitter;
import com.cpip.common.qo.ForgetQo;
import com.cpip.user.mapper.ApplyInformationMapper;
import com.cpip.user.mapper.SubmitterMapper;
import com.cpip.user.service.SubmitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class SubmitterServiceImpl implements SubmitterService {
    @Autowired
    SubmitterMapper submitterMapper;
    @Autowired
    private ApplyInformationMapper applyInformationMapper;

    @Override
    public Submitter selectOne(String account) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Submitter submitter = submitterMapper.selectOne(wrapper);
        if (submitter.isSubmitterState()) {
            return submitter;
        } else {
            return null;
        }
    }

    @Override
    public boolean selectAccount(String account) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Submitter submitter = submitterMapper.selectOne(wrapper);
        if (submitter != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean forgetPassword(ForgetQo forgetQo) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", forgetQo.getAccount());
        Submitter submitter = submitterMapper.selectOne(wrapper);
        if (!submitter.getContact().equals(forgetQo.getContact())) {
            return false;
        }
        submitter.setSubmitterPassword(forgetQo.getPassword());
        submitterMapper.updateById(submitter);
        return true;
    }

    @Override
    public Page<ApplyInformation> selectApplyInformation(int submitterId, int current, int pageSize, String query) {
        Page<ApplyInformation> page = new Page<>(current, pageSize);
        if (StringUtils.isEmpty(query)) {
            QueryWrapper<ApplyInformation> wrapper = new QueryWrapper<>();
            wrapper.eq("submitter_id", submitterId);
            return applyInformationMapper.selectPage(page, wrapper);
        } else {
            QueryWrapper<ApplyInformation> wrapper = new QueryWrapper<>();
            wrapper.eq("submitter_id", submitterId)
                    .like("goods_name", query)
                    .or().like("buyer_name", query);
            return applyInformationMapper.selectPage(page, wrapper);
        }
    }

    @Override
    public ApplyInformation selectApplyInformationById(int applyInformationId) {
        return applyInformationMapper.selectById(applyInformationId);
    }

    @Override
    public boolean updateApplyInformation(ApplyInformation applyInformation) {
        if (applyInformation.getBuyerName() != null) {
            return false;
        } else {
            applyInformationMapper.updateById(applyInformation);
            return true;
        }
    }

    @Override
    public int deleteApplyInformation(int applyInformationId) {
        return applyInformationMapper.deleteById(applyInformationId);
    }

    @Override
    public boolean editPassword(String account, String currentPassword, String newPassword) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Submitter submitter = submitterMapper.selectOne(wrapper);
        if (!submitter.getSubmitterPassword().equals(currentPassword)) {
            return false;
        } else {
            submitter.setSubmitterPassword(newPassword);
            submitterMapper.update(submitter, wrapper);
            return true;
        }
    }

    @Override
    public void editMessage(String account, String submitterNickname, String contact) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Submitter submitter = submitterMapper.selectOne(wrapper);
        submitter.setSubmitterNickname(submitterNickname);
        submitter.setContact(contact);
        submitterMapper.update(submitter, wrapper);
    }

    @Override
    public Page<Submitter> selectAll(int current, int pageSize, String query) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        Page<Submitter> page = new Page<>(current, pageSize);
        if (StringUtils.isEmpty(query)) {
            return submitterMapper.selectPage(page, null);
        } else {
            wrapper.like("account", query)
                    .or().like("submitter_name", query)
                    .or().like("submitter_nickname", query)
                    .or().like("submitter_identity", query);
            return submitterMapper.selectPage(page, wrapper);
        }
    }

    @Override
    public void regist(Submitter submitter) {
        submitter.setSubmitterState(true);
        submitterMapper.insert(submitter);
    }

    @Override
    public void update(Submitter submitter) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", submitter.getAccount());
        submitterMapper.update(submitter, wrapper);
    }

    @Override
    public int delete(String account) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        return submitterMapper.delete(wrapper);
    }

    @Override
    public Submitter login(String account, String submitterPassword) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        wrapper.eq("submitter_password", submitterPassword);
        Submitter submitter = submitterMapper.selectOne(wrapper);
        if (submitter != null && submitter.isSubmitterState()) {
            return submitter;
        } else {
            return null;
        }
    }

    @Override
    public void updateState(String account, boolean state) {
        QueryWrapper<Submitter> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Submitter submitter = submitterMapper.selectOne(wrapper);
        submitter.setSubmitterState(state);
        submitterMapper.updateById(submitter);
    }

    @Override
    public boolean saveApplyInformation(ApplyInformation applyInformation) {
        Submitter submitter = submitterMapper.selectById(applyInformation.getSubmitterId());
        if (submitter.getContact().isEmpty()){
            return false;
        }
        applyInformation.setStatus("待处理");
        applyInformation.setSubmitterName(submitter.getSubmitterName());
        applyInformation.setSubmitterIdentity(submitter.getSubmitterIdentity());
        applyInformation.setSubmitterContact(submitter.getContact());
        applyInformationMapper.insert(applyInformation);
        return true;
    }
}
