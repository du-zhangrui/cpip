package com.cpip.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.ApplyInformation;
import com.cpip.common.pojo.Buyer;
import com.cpip.common.qo.ForgetQo;

public interface BuyerService {

    Page<Buyer> selectAll(int current, int pageSize, String query);

    void regist(Buyer buyer);

    void update(Buyer buyer);

    int delete(String account);

    Buyer login(String account, String buyerPassword);

    Buyer selectOne(String account);

    void updateState(String account, boolean state);

    boolean updateApplyInformation(int buyerId,int applyInformationId);

    boolean selectAccount(String account);

    boolean forgetPassword(ForgetQo forgetQo);

    Page<ApplyInformation> selectApplyInformation(int buyerId, int current, int pageSize, String query);

    boolean editPassword(String account, String currentPassword, String newPassword);

    void editMessage(String account, String buyerName, String contact);

    void addMoney(int buyerId, int money);
}
