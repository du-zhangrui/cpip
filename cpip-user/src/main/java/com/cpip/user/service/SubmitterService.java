package com.cpip.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.ApplyInformation;
import com.cpip.common.pojo.Submitter;
import com.cpip.common.qo.ForgetQo;

public interface SubmitterService {
    void regist(Submitter submitter);

    Submitter selectOne(String accountId);

    Page<Submitter> selectAll(int current, int pageSize, String query);

    void update(Submitter submitter);

    int delete(String account);

    Submitter login(String account, String submitterPassword);

    void updateState(String account, boolean state);

    boolean saveApplyInformation(ApplyInformation applyInformation);

    boolean selectAccount(String account);

    boolean forgetPassword(ForgetQo forgetQo);

    Page<ApplyInformation> selectApplyInformation(int submitterId, int current, int pageSize, String query);

    ApplyInformation selectApplyInformationById(int applyInformationId);

    boolean updateApplyInformation(ApplyInformation applyInformation);

    int deleteApplyInformation(int applyInformationId);

    boolean editPassword(String account, String currentPassword, String newPassword);

    void editMessage(String account, String submitterNickname, String contact);
}
