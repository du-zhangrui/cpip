package com.cpip.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Manufacturer;
import com.cpip.common.qo.ForgetQo;

public interface ManufacturerService {
    Page<Manufacturer> selectAll(int current, int pageSize, String query);

    void regist(Manufacturer manufacturer);

    void update(Manufacturer manufacturer);

    int delete(String account);

    Manufacturer login(String account, String manufacturerPassword);

    Manufacturer selectOne(String account);

    void updateState(String account, boolean state);

    boolean selectAccount(String account);

    boolean forgetPassword(ForgetQo forgetQo);

    boolean editPassword(String account, String currentPassword, String newPassword);

    void editMessage(String account, String manufacturerAddress, String contact);
}
