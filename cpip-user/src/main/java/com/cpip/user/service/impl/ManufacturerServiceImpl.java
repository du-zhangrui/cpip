package com.cpip.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Manufacturer;
import com.cpip.common.qo.ForgetQo;
import com.cpip.user.mapper.ManufacturerMapper;
import com.cpip.user.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {
    @Autowired
    private ManufacturerMapper manufacturerMapper;

    @Override
    public Page<Manufacturer> selectAll(int current, int pageSize, String query) {
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        Page<Manufacturer> page = new Page<>(current, pageSize);
        if (StringUtils.isEmpty(query)) {
            return manufacturerMapper.selectPage(page, null);
        } else {
            wrapper.like("account", query)
                    .or().like("manufacturer_name", query)
                    .or().like("manufacturer_address", query);
            return manufacturerMapper.selectPage(page, wrapper);
        }
    }

    @Override
    public void regist(Manufacturer manufacturer) {
        manufacturer.setManufacturerState(true);
        manufacturerMapper.insert(manufacturer);
    }

    @Override
    public void update(Manufacturer manufacturer) {
        Manufacturer manufacturer1 = manufacturerMapper.selectById(manufacturer.getManufacturerId());
        manufacturer.setMoney(manufacturer1.getMoney());
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", manufacturer.getAccount());
        manufacturerMapper.update(manufacturer, wrapper);
    }

    @Override
    public int delete(String account) {
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        return manufacturerMapper.delete(wrapper);
    }

    @Override
    public Manufacturer login(String account, String manufacturerPassword) {
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        wrapper.eq("manufacturer_password", manufacturerPassword);
        Manufacturer manufacturer = manufacturerMapper.selectOne(wrapper);
        if (manufacturer != null && manufacturer.isManufacturerState()) {
            return manufacturer;
        } else {
            return null;
        }
    }

    @Override
    public Manufacturer selectOne(String account) {
        QueryWrapper<Manufacturer> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account", account);
        Manufacturer manufacturer = manufacturerMapper.selectOne(queryWrapper);
        if (manufacturer.isManufacturerState()) {
            return manufacturer;
        } else {
            return null;
        }
    }

    @Override
    public boolean selectAccount(String account) {
        QueryWrapper<Manufacturer> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account", account);
        Manufacturer manufacturer = manufacturerMapper.selectOne(queryWrapper);
        if (manufacturer != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean forgetPassword(ForgetQo forgetQo) {
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", forgetQo.getAccount());
        Manufacturer manufacturer = manufacturerMapper.selectOne(wrapper);
        if (!manufacturer.getContact().equals(forgetQo.getContact())) {
            return false;
        }
        manufacturer.setManufacturerPassword(forgetQo.getPassword());
        manufacturerMapper.updateById(manufacturer);
        return true;
    }

    @Override
    public boolean editPassword(String account, String currentPassword, String newPassword) {
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Manufacturer manufacturer = manufacturerMapper.selectOne(wrapper);
        if (!manufacturer.getManufacturerPassword().equals(currentPassword)) {
            return false;
        } else {
            manufacturer.setManufacturerPassword(newPassword);
            manufacturerMapper.update(manufacturer, wrapper);
            return true;
        }
    }

    @Override
    public void editMessage(String account, String manufacturerAddress, String contact) {
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Manufacturer manufacturer = manufacturerMapper.selectOne(wrapper);
        manufacturer.setManufacturerAddress(manufacturerAddress);
        manufacturer.setContact(contact);
        manufacturerMapper.update(manufacturer, wrapper);
    }

    @Override
    public void updateState(String account, boolean state) {
        QueryWrapper<Manufacturer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Manufacturer manufacturer = manufacturerMapper.selectOne(wrapper);
        manufacturer.setManufacturerState(state);
        manufacturerMapper.updateById(manufacturer);
    }
}
