package com.cpip.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.ApplyInformation;
import com.cpip.common.pojo.Buyer;
import com.cpip.common.qo.ForgetQo;
import com.cpip.user.mapper.ApplyInformationMapper;
import com.cpip.user.mapper.BuyerMapper;
import com.cpip.user.service.BuyerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class BuyerServiceImpl implements BuyerService {
    @Autowired
    private BuyerMapper buyerMapper;
    @Autowired
    private ApplyInformationMapper applyInformationMapper;

    @Override
    public Page<Buyer> selectAll(int current, int pageSize, String query) {
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        Page<Buyer> page = new Page<>(current, pageSize);
        if (StringUtils.isEmpty(query)) {
            return buyerMapper.selectPage(page, null);
        } else {
            wrapper.like("account", query)
                    .or().like("buyer_name", query)
                    .or().like("buyer_address", query);
            return buyerMapper.selectPage(page, wrapper);
        }
    }

    @Override
    public void regist(Buyer buyer) {
        buyer.setBuyerAddress("山西省运城市盐湖区复旦西街1155号");
        buyer.setBuyerState(true);
        buyerMapper.insert(buyer);
    }

    @Override
    public void update(Buyer buyer) {
        Buyer buyer1 = buyerMapper.selectById(buyer.getBuyerId());
        buyer.setMoney(buyer1.getMoney());
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", buyer.getAccount());
        buyerMapper.update(buyer, wrapper);
    }

    @Override
    public int delete(String account) {
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        return buyerMapper.delete(wrapper);
    }

    @Override
    public Buyer login(String account, String buyerPassword) {
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        wrapper.eq("buyer_password", buyerPassword);
        Buyer buyer = buyerMapper.selectOne(wrapper);
        if (buyer != null && buyer.isBuyerState()) {
            return buyer;
        } else {
            return null;
        }
    }

    @Override
    public Buyer selectOne(String account) {
        QueryWrapper<Buyer> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account", account);
        Buyer buyer = buyerMapper.selectOne(queryWrapper);
        if (buyer != null) {
            return buyer;
        } else {
            return null;
        }
    }

    @Override
    public boolean selectAccount(String account) {
        QueryWrapper<Buyer> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account", account);
        Buyer buyer = buyerMapper.selectOne(queryWrapper);
        if (buyer != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean forgetPassword(ForgetQo forgetQo) {
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", forgetQo.getAccount());
        Buyer buyer = buyerMapper.selectOne(wrapper);
        if (!buyer.getContact().equals(forgetQo.getContact())) {
            return false;
        }
        buyer.setBuyerPassword(forgetQo.getPassword());
        buyerMapper.updateById(buyer);
        return true;
    }

    @Override
    public Page<ApplyInformation> selectApplyInformation(int buyerId, int current, int pageSize, String query) {
        Page<ApplyInformation> page = new Page<>(current, pageSize);
        if (StringUtils.isEmpty(query)) {
            QueryWrapper<ApplyInformation> wrapper = new QueryWrapper<>();
            wrapper.eq("buyer_id", buyerId)
                    .or().isNull("buyer_name");
            return applyInformationMapper.selectPage(page, wrapper);
        } else {
            QueryWrapper<ApplyInformation> wrapper = new QueryWrapper<>();
            wrapper.eq("buyer_id", buyerId)
                    .like("goods_name", query)
                    .or().like("submitter_name", query)
                    .or().like("submitter_identity", query);
            wrapper.or().isNull("buyer_name")
                    .like("goods_name", query)
                    .or().like("submitter_name", query)
                    .or().like("submitter_identity", query);
            return applyInformationMapper.selectPage(page, wrapper);
        }
    }

    @Override
    public boolean editPassword(String account, String currentPassword, String newPassword) {
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Buyer buyer = buyerMapper.selectOne(wrapper);
        if (!buyer.getBuyerPassword().equals(currentPassword)) {
            return false;
        } else {
            buyer.setBuyerPassword(newPassword);
            buyerMapper.update(buyer, wrapper);
            return true;
        }
    }

    @Override
    public void editMessage(String account, String buyerName, String contact) {
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Buyer buyer = buyerMapper.selectOne(wrapper);
        buyer.setBuyerName(buyerName);
        buyer.setContact(contact);
        buyerMapper.update(buyer, wrapper);
    }

    @Override
    public void addMoney(int buyerId, int money) {
        Buyer buyer = buyerMapper.selectById(buyerId);
        buyer.setMoney(buyer.getMoney() + money);
        buyerMapper.updateById(buyer);
    }


    @Override
    public void updateState(String account, boolean state) {
        QueryWrapper<Buyer> wrapper = new QueryWrapper<>();
        wrapper.eq("account", account);
        Buyer buyer = buyerMapper.selectOne(wrapper);
        buyer.setBuyerState(state);
        buyerMapper.updateById(buyer);
    }

    @Override
    public boolean updateApplyInformation(int buyerId, int applyInformationId) {
        Buyer buyer = buyerMapper.selectById(buyerId);
        if (buyer.getContact().isEmpty()) {
            return false;
        }
        ApplyInformation applyInformation = applyInformationMapper.selectById(applyInformationId);
        applyInformation.setStatus("已接单");
        applyInformation.setBuyerId(buyerId);
        applyInformation.setBuyerName(buyer.getBuyerName());
        applyInformation.setBuyerContact(buyer.getContact());
        applyInformationMapper.updateById(applyInformation);
        return true;
    }

}
