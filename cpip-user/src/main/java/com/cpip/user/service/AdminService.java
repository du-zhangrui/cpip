package com.cpip.user.service;

import com.cpip.common.pojo.Admin;

public interface AdminService {
    void update(Admin admin);

    Admin login(String account, String adminPassword);
}
