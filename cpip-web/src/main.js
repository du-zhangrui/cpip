import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueParticles from 'vue-particles'
import './assets/fonts/iconfont.css'
import axios from "axios";
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'

Vue.prototype.$http = axios
Vue.use(VueParticles)
Vue.use(ElementUI);
Vue.config.productionTip = false
// 将富文本编辑器，注册为全局可用的组件
Vue.use(VueQuillEditor)
Vue.filter('dateFormat', function(originVal) {
  const dt = new Date(originVal)

  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '0')
  const hh = (dt.getHours() + '').padStart(2, '0')
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  const ss = (dt.getSeconds() + '').padStart(2, '0')

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})
Vue.filter('day', function(originVal) {
  const dt = new Date(originVal)
  const d = (dt.getDate() + '').padStart(2, '0')
  return `${d}`
})


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

