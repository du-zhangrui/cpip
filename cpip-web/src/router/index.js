import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/login/Login.vue'
import AdminIndex from "../components/admin/AdminIndex";
import ManufacturerIndex from "../components/manufacturer/ManufacturerIndex";
import SubmitterIndex from "../components/submitter/SubmitterIndex";
import BuyerIndex from "../components/buyer/BuyerIndex";
import Welcome from "../components/forgetPassword/ForgetPassword";
import ForgetPassword from "../components/forgetPassword/ForgetPassword";
import Manufacturers from "../components/manufacturer/Manufacturers";
import Submitters from "../components/submitter/Submitters";
import Buyers from "../components/buyer/Buyers";
import Goods from "../components/goods/Goods";
import Orders from "../components/orders/Orders";
import ManufacturerInformation from "../components/manufacturer/ManufacturerInformation";
import GoodsManagement from "../components/goods/GoodsManagement";
import ApplyInformation from "../components/applyInformation/ApplyInformation";
import SubmitterInformation from "../components/submitter/SubmitterInformation";
import BuyerInformation from "../components/buyer/BuyerInformation";
import SubmitterrApply from "../components/applyInformation/SubmitterrApply";
import GoodsApply from "../components/goods/GoodsApply";
import Cart from "../components/cart/Cart";
import BuyerOrder from "../components/orders/BuyerOrder";
import OrdersManagement from "../components/orders/OrdersManagement";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {path: '/', redirect: '/login'},
        {path: '/login', component: Login},
        {path: '/forgetPassword', component: ForgetPassword},
        {
            path: '/adminIndex',
            component: AdminIndex,
            redirect: '/manufacturers',
            children: [
                {path: '/manufacturers', component: Manufacturers},
                {path: '/submitters', component: Submitters},
                {path: '/buyers', component: Buyers},
                {path: '/goods', component: Goods},
                {path: '/orders', component: Orders}
            ]
        },
        {
            path: '/manufacturerIndex',
            component: ManufacturerIndex,
            redirect: '/manufacturerInformation',
            children: [
                {path: '/manufacturerInformation', component: ManufacturerInformation},
                {path: '/goodsManagement', component: GoodsManagement},
                {path: '/ordersManagement', component: OrdersManagement}
            ]
        },
        {
            path: '/submitterIndex',
            component: SubmitterIndex,
            redirect: '/submitterInformation',
            children: [
                {path: '/submitterInformation', component: SubmitterInformation},
                {path: '/applyInformation', component: ApplyInformation}
            ]
        },
        {
            path: '/buyerIndex',
            component: BuyerIndex,
            redirect: '/buyerInformation',
            children: [
                {path: '/buyerInformation', component: BuyerInformation},
                {path: '/submitterApply', component: SubmitterrApply},
                {path: '/goodsApply', component: GoodsApply},
                {path: '/cart', component: Cart},
                {path: '/buyerOrder', component: BuyerOrder}
            ]
        }
    ]
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
    if (to.path === '/login') return next()
    if (to.path === '/forgetPassword') return next()
    // 获取token
    const tokenStr = window.sessionStorage.getItem('token')
    if (!tokenStr) return next('/login')
    next()
})

export default router
