let proxyObj={}

proxyObj['/user']={
    //websocket
    ws:false,
    //目标地址
    target:'http://localhost:10001/',
    //将请求头host设置成target
    changeOrigin:true,
    pathRewrite:{
        '^/':'/'
    }
}

proxyObj['/goods']={
    //websocket
    ws:false,
    //目标地址
    target:'http://localhost:10002/',
    //将请求头host设置成target
    changeOrigin:true,
    pathRewrite:{
        '^/':'/'
    }
}

proxyObj['/cart']={
    //websocket
    ws:false,
    //目标地址
    target:'http://localhost:10003/',
    //将请求头host设置成target
    changeOrigin:true,
    pathRewrite:{
        '^/':'/'
    }
}

proxyObj['/orders']={
    //websocket
    ws:false,
    //目标地址
    target:'http://localhost:10004/',
    //将请求头host设置成target
    changeOrigin:true,
    pathRewrite:{
        '^/':'/'
    }
}


module.exports={
    devServer:{
        host:'localhost',
        port:8080,
        proxy:proxyObj
    }
}
