package com.cpip.cart.service;

import com.cpip.common.pojo.Cart;

import java.util.List;

public interface CartService {
    List<Cart> selectCart(int buyerId);

    void add(Cart cart);

    void update(Long cartId, String status);

    int delete(int cartId);

    int selectNumber(int buyerId, int goodsId, int manufacturerId);
}
