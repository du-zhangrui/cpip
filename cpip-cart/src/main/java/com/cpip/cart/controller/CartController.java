package com.cpip.cart.controller;

import com.cpip.cart.service.CartService;
import com.cpip.common.pojo.Cart;
import com.cpip.common.vo.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    //获取购物车数量图标数字
    @GetMapping("/selectNumber")
    public DataResponse selectNumber(@RequestParam int buyerId, @RequestParam int goodsId, @RequestParam int manufactirerId) {
        try {
            int number = cartService.selectNumber(buyerId, goodsId, manufactirerId);
            return DataResponse.success(number);
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("查询失败");
        }
    }

    //根据购物车id查询货物
    @GetMapping("/selectCart")
    public DataResponse selectCart(@RequestParam int buyerId) {
        List<Cart> cartList = cartService.selectCart(buyerId);
        if (cartList != null) {
            return DataResponse.success(cartList);
        } else {
            return DataResponse.error("查询失败");
        }
    }

    //添加
    @PostMapping("/save")
    public DataResponse add(@RequestBody Cart cart) {
        try {
            cartService.add(cart);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("添加失败");
        }
    }

    //修改
    @PutMapping("/update")
    public DataResponse update(@RequestBody Cart cart) {
        try {
            cartService.update(cart.getCartId(), cart.getStatus());
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //根据购物车id删除
    @DeleteMapping("/delete")
    public DataResponse delete(@RequestParam int cartId) {
        int result = cartService.delete(cartId);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("删除失败");
        }
    }
}
