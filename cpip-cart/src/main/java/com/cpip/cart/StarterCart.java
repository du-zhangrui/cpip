package com.cpip.cart;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cpip.cart.mapper")
public class StarterCart {
    public static void main(String[] args) {
        SpringApplication.run(StarterCart.class, args);
    }
}
