package com.cpip.cart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cpip.common.pojo.Goods;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodsMapper extends BaseMapper<Goods> {
}
