package com.cpip.order.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Cart;
import com.cpip.common.pojo.Orders;
import com.cpip.common.vo.DataResponse;
import com.cpip.order.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    //查询所有订单
    @GetMapping("/selectAll")
    public DataResponse selectAll(@RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Orders> ordersPage = ordersService.selectAll(current, pageSize, query);
        if (ordersPage != null) {
            return DataResponse.success(ordersPage);
        } else {
            return DataResponse.error("查询失败");
        }
    }

    //根据买家id查询订单(逻辑查找)
    @GetMapping("selectOrdersByBuyerId")
    public DataResponse selectOrdersByBuyerId(@RequestParam int buyerId, @RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Orders> ordersPage = ordersService.selectOrdersByBuyerId(buyerId, current, pageSize, query);
        if (ordersPage != null) {
            return DataResponse.success(ordersPage);
        } else {
            return DataResponse.error("查询失败");
        }
    }

    //根据厂家id查询货物
    @GetMapping("/selectOrdersByManufacturerId")
    public DataResponse selectOrdersByManufacturerId(@RequestParam int manufacturerId, @RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Orders> ordersPage = ordersService.selectOrdersByManufacturerId(manufacturerId, current, pageSize, query);
        if (ordersPage != null) {
            return DataResponse.success(ordersPage);
        } else {
            return DataResponse.error("查询失败");
        }
    }

    //添加
    @PostMapping("/save")
    public DataResponse add(@RequestBody Cart cart) {
        String result = ordersService.add(cart.getCartId());
        if (result.equals("结算成功")) {
            return DataResponse.success("结算成功！");
        } else {
            return DataResponse.error(result);
        }
    }

    //修改
    @GetMapping("/update")
    public DataResponse update(@RequestParam Long orderId, @RequestParam String status) {
        try {
            ordersService.update(orderId, status);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //退货
    @GetMapping("/returnGoods")
    public DataResponse applyReturnGoods(@RequestParam Long orderId, @RequestParam String status) {
        try {
            ordersService.returnGoods(orderId, status);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("取消订单操作失败");
        }
    }

    //根据buyerId删除订单
    @DeleteMapping("/delete")
    public DataResponse delete(@RequestParam int buyerId) {
        int result = ordersService.delete(buyerId);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("删除失败");
        }
    }

    //退货
    @DeleteMapping("/receiveReturnGoods")
    public DataResponse receiveReturnGoods(@RequestParam Long orderId) {
        int result = ordersService.receiveReturnGoods(orderId);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("操作失败");
        }
    }
}
