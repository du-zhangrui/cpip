package com.cpip.order.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Orders;
import org.springframework.web.bind.annotation.RequestParam;

public interface OrdersService {
    Page<Orders> selectAll(int current, int pageSize, String query);

    Page<Orders> selectOrdersByBuyerId(int buyerId, int current, int pageSize, String query);

    String add(Long cartId);

    void update(Long orderId, String status);

    int delete(int ordersId);

    Page<Orders> selectOrdersByManufacturerId(int manufacturerId, @RequestParam int current, @RequestParam int pageSize, String query);

    void returnGoods(Long orderId, String status);

    int receiveReturnGoods(Long orderId);
}
