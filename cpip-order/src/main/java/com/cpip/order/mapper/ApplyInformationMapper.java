package com.cpip.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cpip.common.pojo.ApplyInformation;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplyInformationMapper extends BaseMapper<ApplyInformation> {
}
