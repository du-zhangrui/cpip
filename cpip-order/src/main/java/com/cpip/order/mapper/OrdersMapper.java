package com.cpip.order.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.cpip.common.pojo.Orders;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersMapper extends BaseMapper<Orders> {
    @Select("select * from orders ${ew.customSqlSegment}")
    List<Orders> mySelectList(IPage<Orders> page, @Param(Constants.WRAPPER) Wrapper<Orders> ordersWrapper);
}
