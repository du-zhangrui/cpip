package com.cpip.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cpip.common.pojo.Buyer;
import org.springframework.stereotype.Repository;

@Repository
public interface BuyerMapper extends BaseMapper<Buyer>{
}
