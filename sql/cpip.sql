/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.20-log : Database - cpip
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cpip` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cpip`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `account` varchar(20) NOT NULL COMMENT '管理员account',
  `admin_name` varchar(20) NOT NULL COMMENT '管理员name',
  `admin_password` varchar(20) NOT NULL COMMENT '管理员password',
  `contact` varchar(11) NOT NULL COMMENT '管理员联系方式',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`admin_id`,`account`,`admin_name`,`admin_password`,`contact`) values
(1,'admin','管理员','123456','13699999999');

/*Table structure for table `apply_information` */

DROP TABLE IF EXISTS `apply_information`;

CREATE TABLE `apply_information` (
  `apply_information_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '申请信息id',
  `goods_name` varchar(20) NOT NULL COMMENT '货物名',
  `goods_num` int(5) NOT NULL COMMENT '货物数量',
  `status` varchar(20) NOT NULL COMMENT '采购状态',
  `submitter_id` int(5) NOT NULL COMMENT '上报员id',
  `submitter_name` varchar(20) NOT NULL COMMENT '上报员name',
  `submitter_identity` varchar(20) NOT NULL COMMENT '上报员身份',
  `submitter_contact` varchar(11) NOT NULL COMMENT '上报员联系方式',
  `buyer_id` int(5) DEFAULT NULL COMMENT '采购员id',
  `buyer_name` varchar(20) DEFAULT NULL COMMENT '采购员name',
  `buyer_contact` varchar(11) DEFAULT NULL COMMENT '采购员联系方式',
  PRIMARY KEY (`apply_information_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `apply_information` */

insert  into `apply_information`(`apply_information_id`,`goods_name`,`goods_num`,`status`,`submitter_id`,`submitter_name`,`submitter_identity`,`submitter_contact`,`buyer_id`,`buyer_name`,`buyer_contact`) values
(1,'足球',5,'已完成',7,'马飞','教导主任','17835698352',1,'采购员一','17835389283'),
(2,'篮球',3,'已接单',1,'李三','学生','13923654933',3,'采购员三','17746375968'),
(3,'足球',5,'已完成',1,'李三','学生','13923654933',1,'采购员一','17835389285'),
(5,'大学语文',15,'已完成',1,'李三','学生','13923654933',9,'采购员六','15909891241'),
(6,'高等数学',15,'已接单',1,'李三','学生','13923654933',2,'采购员二','17893847589'),
(7,'书桌',10,'已完成',13,'陈三一','教导主任','15678989082',1,'采购员一','17835389285'),
(8,'高等数学',20,'已接单',13,'陈三一','教导主任','15678989082',3,'采购员三','17746375968'),
(9,'乒乓球',11,'已接单',13,'陈三一','教导主任','15678989082',1,'采购员一','17835389282'),
(11,'计算机导论',5,'已完成',14,'李娜娜','学生','15676787891',1,'采购员一','17835389282'),
(12,'大学语文',5,'已完成',14,'李娜娜','学生','15676787891',9,'采购员六','15909891241'),
(15,'橡皮',10,'已接单',9,'华晨','教导主任','15612325672',10,'采购员七','15676787890'),
(16,'铅笔',10,'已完成',9,'华晨','教导主任','15612325672',10,'采购员七','15676787890'),
(17,'卷笔刀',10,'待处理',9,'华晨','教导主任','15612325672',0,NULL,NULL),
(18,'计算机导论',10,'待处理',1,'李三','学生','13923654935',0,NULL,NULL),
(19,'乒乓球',5,'已接单',1,'李三','学生','13923654935',1,'采购员一','17835389282'),
(20,'排球',3,'待签收',1,'李三','学生','13923654935',1,'采购员一','17835389282'),
(21,'web前端',5,'待发货',1,'李三','学生','13923654935',1,'采购员一','17835389282'),
(22,'vue框架',10,'已接单',1,'李三','学生','13923654935',1,'采购员一','17835389282');

/*Table structure for table `buyer` */

DROP TABLE IF EXISTS `buyer`;

CREATE TABLE `buyer` (
  `buyer_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '采购员id',
  `account` varchar(30) NOT NULL COMMENT '账号',
  `buyer_name` varchar(30) NOT NULL COMMENT '采购员name',
  `buyer_password` varchar(30) NOT NULL COMMENT '采购员password',
  `buyer_address` varchar(50) NOT NULL COMMENT '采购员address',
  `contact` varchar(11) NOT NULL COMMENT '采购员phone',
  `money` int(10) NOT NULL DEFAULT '0' COMMENT '采购员money',
  `buyer_state` tinyint(1) NOT NULL COMMENT '账号是否可用',
  PRIMARY KEY (`buyer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `buyer` */

insert  into `buyer`(`buyer_id`,`account`,`buyer_name`,`buyer_password`,`buyer_address`,`contact`,`money`,`buyer_state`) values
(1,'caigouyuan1','采购员一','123456','山西省运城市盐湖区复旦西街1155号','17835389282',1005,1),
(2,'caigouyuan2','采购员二','123456','山西省运城市盐湖区复旦西街1155号','17893847589',1000,1),
(3,'caigouyuan3','采购员三','123456','山西省运城市盐湖区复旦西街1155号','17746375968',9887,1),
(5,'caigouyuan4','采购员四','123456','山西省运城市盐湖区复旦西街1155号','17692343238',1000,1),
(8,'caigouyuan5','采购员五','123456','山西省运城市盐湖区复旦西街1155号','17623454673',0,0),
(9,'caigouyuan6','采购员六','123456','山西省运城市盐湖区复旦西街1155号','15909891241',92,1),
(10,'caigouyuan7','采购员七','123456','山西省运城市盐湖区复旦西街1155号','15676787890',980,1);

/*Table structure for table `cart` */

DROP TABLE IF EXISTS `cart`;

CREATE TABLE `cart` (
  `cart_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '购物车id',
  `goods_id` int(5) NOT NULL COMMENT '货物id',
  `goods_name` varchar(20) NOT NULL COMMENT '货物name',
  `goods_price` int(5) NOT NULL COMMENT '货物价格',
  `goods_num` int(5) NOT NULL COMMENT '货物数量',
  `total_price` int(11) NOT NULL COMMENT '总价',
  `status` varchar(10) NOT NULL COMMENT '购物车status',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `manufacturer_id` int(5) NOT NULL COMMENT '厂家id',
  `manufacturer_name` varchar(20) NOT NULL COMMENT '厂家name',
  `manufacturer_address` varchar(50) NOT NULL COMMENT '厂家address',
  `manufacturer_contact` varchar(11) NOT NULL COMMENT '厂家联系方式',
  `buyer_id` int(5) NOT NULL COMMENT '采购员id',
  `buyer_name` varchar(20) NOT NULL COMMENT '采购员name',
  `buyer_address` varchar(30) NOT NULL COMMENT '采购员address',
  `buyer_contact` varchar(11) NOT NULL COMMENT '采购员联系方式',
  `is_deleted` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/*Data for the table `cart` */

insert  into `cart`(`cart_id`,`goods_id`,`goods_name`,`goods_price`,`goods_num`,`total_price`,`status`,`create_time`,`update_time`,`manufacturer_id`,`manufacturer_name`,`manufacturer_address`,`manufacturer_contact`,`buyer_id`,`buyer_name`,`buyer_address`,`buyer_contact`,`is_deleted`) values
(21,5,'大学语文',31,5,155,'已付款','2021-04-30 17:15:35','2021-04-30 17:15:35',7,'厂家五','江西华东路123号','13567152337',9,'采购员六','山西省运城市盐湖区复旦西街1155号','15909891241',1),
(22,1,'书桌',70,4,280,'待付款','2021-04-30 21:23:09','2021-04-30 21:23:09',3,'厂家三','山西省太原市华信街256号','13902345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(23,2,'篮球',56,4,224,'待付款','2021-05-01 00:12:51','2021-05-01 00:12:51',1,'厂家一','上海青浦区华志路556号','17824359089',9,'采购员六','山西省运城市盐湖区复旦西街1155号','15909891241',1),
(24,2,'篮球',56,1,56,'待付款','2021-05-01 00:16:36','2021-05-01 00:16:36',1,'厂家一','上海青浦区华志路556号','17824359089',9,'采购员六','山西省运城市盐湖区复旦西街1155号','15909891241',1),
(25,2,'篮球',56,1,56,'待付款','2021-05-01 00:16:36','2021-05-01 00:16:36',1,'厂家一','上海青浦区华志路556号','17824359089',9,'采购员六','山西省运城市盐湖区复旦西街1155号','15909891241',1),
(26,2,'篮球',56,1,56,'已付款','2021-05-01 00:16:55','2021-05-01 00:16:55',1,'厂家一','上海青浦区华志路556号','17824359089',9,'采购员六','山西省运城市盐湖区复旦西街1155号','15909891241',1),
(27,1,'书桌',70,4,280,'待付款','2021-05-03 15:54:51','2021-05-03 15:54:51',3,'厂家三','山西省太原市华信街256号','13902345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(28,1,'书桌',70,4,280,'待付款','2021-05-03 15:57:07','2021-05-03 15:57:07',3,'厂家三','山西省太原市华信街256号','13902345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(29,3,'足球',40,5,200,'待付款','2021-05-03 15:59:34','2021-05-03 15:59:34',5,'厂家四','广州华门路123号','13602345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(30,2,'篮球',56,1,56,'待付款','2021-05-03 16:04:04','2021-05-03 16:04:04',1,'厂家一','上海青浦区华志路556号','17824359089',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(31,3,'足球',40,1,40,'待付款','2021-05-03 16:04:11','2021-05-03 16:04:11',5,'厂家四','广州华门路123号','13602345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(32,12,'橡皮擦',1,10,10,'已付款','2021-05-03 21:50:29','2021-05-03 21:50:29',8,'厂家六','北京市朝阳区新华街245号','13145657678',10,'采购员七','山西省运城市盐湖区复旦西街1155号','15676787890',1),
(33,12,'橡皮擦',1,100,100,'待付款','2021-05-03 21:54:13','2021-05-03 21:54:13',8,'厂家六','北京市朝阳区新华街245号','13145657678',10,'采购员七','山西省运城市盐湖区复旦西街1155号','15676787890',1),
(34,14,'铅笔',1,10,10,'已付款','2021-05-03 22:03:23','2021-05-03 22:03:23',8,'厂家六','北京市朝阳区新华街245号','13145657678',10,'采购员七','山西省运城市盐湖区复旦西街1155号','15676787890',1),
(35,1,'书桌',70,1,70,'待付款','2021-05-03 22:19:39','2021-05-03 22:19:39',3,'厂家三','山西省太原市华信街256号','13902345337',10,'采购员七','山西省运城市盐湖区复旦西街1155号','15676787890',0),
(36,3,'足球',40,1,40,'待付款','2021-05-08 21:23:58','2021-05-08 21:23:58',5,'厂家四','广州华门路123号','13602345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(37,2,'篮球',56,3,168,'待付款','2021-05-08 21:24:02','2021-05-08 21:24:02',1,'厂家一','上海青浦区华志路556号','17824359089',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(38,1,'书桌',70,10,700,'已付款','2021-05-08 21:24:06','2021-05-08 21:24:06',3,'厂家三','山西省太原市华信街256号','13902345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(39,3,'足球',40,10,400,'已付款','2021-05-19 20:57:20','2021-05-19 20:57:20',5,'厂家四','广州华门路123号','13602345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',1),
(40,8,'足球',26,15,390,'待付款','2021-05-23 10:07:49','2021-05-23 10:07:49',1,'厂家一','上海青浦区华志路556号','17824359088',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',0),
(41,1,'书桌',70,100,7000,'待付款','2021-05-23 10:08:48','2021-05-23 10:08:48',3,'厂家三','山西省太原市华信街256号','13902345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',0),
(42,11,'计算机导论',30,5,150,'已付款','2021-05-23 10:24:46','2021-05-23 10:24:46',7,'厂家五','江西华东路123号','13567152337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',1),
(43,9,'乒乓球',2,5,10,'已付款','2021-05-23 10:58:25','2021-05-23 10:58:25',1,'厂家一','上海青浦区华志路556号','17824359088',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',1),
(44,15,'web前端',36,5,180,'已付款','2021-05-23 13:28:21','2021-05-23 13:28:21',1,'厂家一','上海青浦区华志路556号','17824359088',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',1),
(45,10,'排球',5,3,15,'已付款','2021-05-23 13:31:49','2021-05-23 13:31:49',1,'厂家一','上海青浦区华志路556号','17824359088',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',1);

/*Table structure for table `goods` */

DROP TABLE IF EXISTS `goods`;

CREATE TABLE `goods` (
  `goods_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '货物id',
  `goods_name` varchar(30) NOT NULL COMMENT '货物name',
  `goods_num` int(5) NOT NULL COMMENT '货物数量',
  `price` int(5) NOT NULL COMMENT '价格',
  `description` varchar(50) NOT NULL COMMENT '描述',
  `manufacturer_id` int(5) NOT NULL COMMENT '厂家id',
  `manufacturer_name` varchar(30) NOT NULL COMMENT '厂家name',
  `manufacturer_address` varchar(50) NOT NULL COMMENT '厂家地址',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `goods` */

insert  into `goods`(`goods_id`,`goods_name`,`goods_num`,`price`,`description`,`manufacturer_id`,`manufacturer_name`,`manufacturer_address`) values
(1,'书桌',990,70,'质量很好',3,'厂家三','山西省太原市华信街256号'),
(2,'篮球',200,56,'结实',1,'厂家一','上海青浦区华志路555号'),
(3,'足球',290,40,'黑白色',5,'厂家四','广州华门路123号'),
(4,'高等数学',1500,33,'纸张质量好',7,'厂家五','江西省华东路123号'),
(5,'大学语文',1600,31,'纸张质量好',7,'厂家五','江西省华东路123号'),
(6,'乒乓球拍',1470,15,'耐打',2,'厂家二','北京朝阳区大兴路234号'),
(7,'大学语文',1470,27,'纸张质量上等，价格实惠，拍下立马发货',7,'厂家五','江西华东路123号'),
(8,'足球',12,26,'弹力十足~~~~~~~~~~~',1,'厂家一','上海青浦区华志路555号'),
(9,'乒乓球',300,2,'价格实惠，质量优佳!!',1,'厂家一','上海青浦区华志路555号'),
(10,'排球',7,5,'',1,'厂家一','上海青浦区华志路555号'),
(11,'计算机导论',25,30,'最新版~~~挥泪大甩卖~~~~~~~~~~~~~',7,'厂家五','江西华东路123号'),
(12,'橡皮擦',90,1,'白白软软滴~~~',8,'厂家六','北京市朝阳区新华街245号'),
(13,'橡皮卷',50,4,'款式多，样式多，价格实惠，大家快来抢购吧',8,'厂家六','北京市朝阳区新华街245号'),
(14,'铅笔',40,1,'质量有保障，样式好看藕！！！',8,'厂家六','北京市朝阳区新华街245号'),
(15,'web前端',35,36,'入门级web书籍，最新版，大家快来抢购吧！！！',1,'厂家一','上海青浦区华志路556号'),
(16,'vue框架',20,45,'适合入门',1,'厂家一','上海青浦区华志路556号');

/*Table structure for table `manufacturer` */

DROP TABLE IF EXISTS `manufacturer`;

CREATE TABLE `manufacturer` (
  `manufacturer_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '厂家id',
  `account` varchar(30) NOT NULL COMMENT '账号',
  `manufacturer_name` varchar(30) NOT NULL COMMENT '厂家name',
  `manufacturer_password` varbinary(30) NOT NULL COMMENT '厂家password',
  `manufacturer_address` varchar(50) NOT NULL COMMENT '厂家address',
  `contact` varchar(11) NOT NULL COMMENT '厂家phone',
  `money` int(10) NOT NULL DEFAULT '0' COMMENT '厂家金额',
  `manufacturer_state` tinyint(1) NOT NULL COMMENT '账号是否可用',
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `manufacturer` */

insert  into `manufacturer`(`manufacturer_id`,`account`,`manufacturer_name`,`manufacturer_password`,`manufacturer_address`,`contact`,`money`,`manufacturer_state`) values
(1,'changjia1','厂家一','123456','上海青浦区华志路556号','17824359088',251,1),
(2,'changjia2','厂家二','123456','北京朝阳区大兴路234号','13908782373',500,1),
(3,'changjia3','厂家三','123456','山西省太原市华信街256号','13902345337',2450,1),
(5,'changjia4','厂家四','123456','广州华门路123号','13602345337',1040,1),
(7,'changjia5','厂家五','123456','江西华东路123号','13567152337',1002,1),
(8,'changjia6','厂家六','123456','北京市朝阳区新华街245号','13145657678',20,1),
(11,'changjia7','厂家七','123456','江苏省落花区北府街132号','13193748561',0,0),
(12,'changjia8','厂家八','123456','','',0,1);

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `orders_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `cart_id` int(5) NOT NULL COMMENT '购物车id',
  `goods_id` int(5) NOT NULL COMMENT '货物id',
  `goods_name` varchar(30) NOT NULL COMMENT '货物name',
  `goods_num` int(5) NOT NULL COMMENT '货物num',
  `goods_price` int(5) NOT NULL COMMENT '单价',
  `total_price` int(5) NOT NULL COMMENT '总价',
  `status` varchar(10) NOT NULL COMMENT '货物状态',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `manufacturer_id` int(5) NOT NULL COMMENT '厂家id',
  `manufacturer_name` varchar(30) NOT NULL COMMENT '厂家name',
  `manufacturer_address` varchar(30) NOT NULL COMMENT '厂家地址',
  `manufacturer_contact` varchar(11) NOT NULL COMMENT '厂家联系方式',
  `buyer_id` int(5) NOT NULL COMMENT '采购员id',
  `buyer_name` varchar(30) NOT NULL COMMENT '采购员name',
  `buyer_address` varchar(30) NOT NULL COMMENT '采购员地址',
  `buyer_contact` varchar(11) NOT NULL COMMENT '采购员联系方式',
  `is_deleted` int(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`orders_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `orders` */

insert  into `orders`(`orders_id`,`cart_id`,`goods_id`,`goods_name`,`goods_num`,`goods_price`,`total_price`,`status`,`create_time`,`update_time`,`manufacturer_id`,`manufacturer_name`,`manufacturer_address`,`manufacturer_contact`,`buyer_id`,`buyer_name`,`buyer_address`,`buyer_contact`,`is_deleted`) values
(15,21,5,'大学语文',5,31,155,'已签收','2021-04-30 17:15:46','2021-04-30 18:15:31',7,'厂家五','江西华东路123号','13567152337',9,'采购员六','山西省运城市盐湖区复旦西街1155号','15909891241',0),
(16,26,2,'篮球',1,56,56,'待发货','2021-05-01 00:17:01','2021-05-01 00:17:01',1,'厂家一','上海青浦区华志路556号','17824359089',9,'采购员六','山西省运城市盐湖区复旦西街1155号','15909891241',0),
(17,32,12,'橡皮擦',10,1,10,'已签收','2021-05-03 21:52:11','2021-05-03 22:10:29',8,'厂家六','北京市朝阳区新华街245号','13145657678',10,'采购员七','山西省运城市盐湖区复旦西街1155号','15676787890',0),
(18,34,14,'铅笔',10,1,10,'待发货','2021-05-03 22:03:38','2021-05-03 22:03:38',8,'厂家六','北京市朝阳区新华街245号','13145657678',10,'采购员七','山西省运城市盐湖区复旦西街1155号','15676787890',0),
(19,39,3,'足球',10,40,400,'待发货','2021-05-19 20:57:34','2021-05-19 20:57:34',5,'厂家四','广州华门路123号','13602345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',0),
(20,38,1,'书桌',10,70,700,'已签收','2021-05-19 21:01:51','2021-05-19 21:04:17',3,'厂家三','山西省太原市华信街256号','13902345337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389285',0),
(21,42,11,'计算机导论',5,30,150,'已签收','2021-05-23 10:24:52','2021-05-23 10:36:40',7,'厂家五','江西华东路123号','13567152337',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',0),
(22,43,9,'乒乓球',5,2,10,'退货运输中','2021-05-23 10:59:16','2021-05-23 17:19:01',1,'厂家一','上海青浦区华志路556号','17824359088',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',1),
(23,44,15,'web前端',5,36,180,'待发货','2021-05-23 13:29:35','2021-05-23 13:29:35',1,'厂家一','上海青浦区华志路556号','17824359088',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',0),
(24,45,10,'排球',3,5,15,'运输中','2021-05-23 13:32:44','2021-05-23 13:33:00',1,'厂家一','上海青浦区华志路556号','17824359088',1,'采购员一','山西省运城市盐湖区复旦西街1155号','17835389282',0);

/*Table structure for table `submitter` */

DROP TABLE IF EXISTS `submitter`;

CREATE TABLE `submitter` (
  `submitter_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `account` varchar(30) NOT NULL COMMENT '账号',
  `submitter_name` varchar(30) NOT NULL COMMENT '上报员name',
  `submitter_password` varchar(30) NOT NULL COMMENT '上报员password',
  `submitter_nickname` varchar(30) NOT NULL COMMENT '上报员nickname',
  `submitter_identity` varchar(30) NOT NULL COMMENT '上报员身份',
  `contact` varchar(11) NOT NULL COMMENT '上报员phone',
  `submitter_state` tinyint(1) NOT NULL COMMENT '账号是否可用',
  PRIMARY KEY (`submitter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `submitter` */

insert  into `submitter`(`submitter_id`,`account`,`submitter_name`,`submitter_password`,`submitter_nickname`,`submitter_identity`,`contact`,`submitter_state`) values
(1,'lisan','李三','123456','大飞鱼','学生','13923654935',1),
(2,'lixue','李雪','123456','落雪','老师','13698780121',1),
(3,'chenyan','陈燕','123456','飞燕','老师','17835678722',0),
(7,'mafei','马飞','1234567','飞马','教导主任','17835698353',0),
(8,'liuchen','刘晨','123456','溜达猪','学生','15987651290',1),
(9,'huachen','华晨','123456','','教导主任','15612325672',1),
(13,'chensanyi','陈三一','123456','三亿人民币','教导主任','15678989082',1),
(14,'linana','李娜娜','123456','我是一朵花','学生','15676787891',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
