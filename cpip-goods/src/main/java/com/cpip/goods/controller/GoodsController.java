package com.cpip.goods.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Goods;
import com.cpip.common.vo.DataResponse;
import com.cpip.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    //查询所有
    @GetMapping("/selectAll")
    public DataResponse selectAll(@RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Goods> goodsPage = goodsService.selectAll(current, pageSize, query);
        if (goodsPage != null) {
            return DataResponse.success(goodsPage);
        } else {
            return DataResponse.error("查询不到货物");
        }
    }

    //根据厂家id查询货物
    @GetMapping("/selectAllByManufacturerId")
    public DataResponse selectAllByManufacturerId(@RequestParam int manufacturerId, @RequestParam int current, @RequestParam int pageSize, String query) {
        Page<Goods> goodsPage = goodsService.selectAllByManufacturerId(manufacturerId, current, pageSize, query);
        if (goodsPage != null) {
            return DataResponse.success(goodsPage);
        } else {
            return DataResponse.error("查询不到货物");
        }
    }

    //根据货物id查询货物
    @GetMapping("/selectByGoodsId")
    public DataResponse selectByGoodsId(@RequestParam int goodsId) {
        Goods goods = goodsService.selectByGoodsId(goodsId);
        if (goods != null) {
            return DataResponse.success(goods);
        } else {
            return DataResponse.error("查询不到货物");
        }
    }

    //根据商品名相似度搜索货物
    @GetMapping("/selectSomeByName")
    public DataResponse selectSomeByName(String name) {
        List<Goods> goodsList = goodsService.selectSomeByName(name);
        if (goodsList != null) {
            return DataResponse.success(goodsList);
        } else {
            return DataResponse.error("查询不到货物");
        }
    }

    //添加
    @PostMapping("/add")
    public DataResponse add(@RequestBody Goods goods) {
        try {
            goodsService.add(goods);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("添加失败");
        }
    }

    //修改
    @PutMapping("/update")
    public DataResponse update(@RequestBody Goods goods) {
        try {
            goodsService.update(goods);
            return DataResponse.success();
        } catch (Exception e) {
            e.printStackTrace();
            return DataResponse.error("修改失败");
        }
    }

    //删除
    @DeleteMapping("/delete")
    public DataResponse delete(@RequestParam String goodsId) {
        int result = goodsService.delete(goodsId);
        if (result == 1) {
            return DataResponse.success();
        } else {
            return DataResponse.error("删除失败");
        }
    }
}
