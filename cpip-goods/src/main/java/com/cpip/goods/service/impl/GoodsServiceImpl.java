package com.cpip.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Goods;
import com.cpip.common.pojo.Manufacturer;
import com.cpip.goods.mapper.GoodsMapper;
import com.cpip.goods.mapper.ManufacturerMapper;
import com.cpip.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private ManufacturerMapper manufacturerMapper;

    @Override
    public Page<Goods> selectAll(int current, int pageSize, String query) {
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        Page<Goods> page = new Page<>(current, pageSize);
        if (StringUtils.isEmpty(query)) {
            return goodsMapper.selectPage(page, null);
        } else {
            wrapper.like("goods_name", query)
                    .or().like("description", query)
                    .or().like("manufacturer_name", query)
                    .or().like("manufacturer_address", query);
            return goodsMapper.selectPage(page, wrapper);
        }
    }

    @Override
    public Page<Goods> selectAllByManufacturerId(int manufacturerId, int current, int pageSize, String query) {
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        Page<Goods> page = new Page<>(current, pageSize);
        if (StringUtils.isEmpty(query)) {
            wrapper.eq("manufacturer_id", manufacturerId);
            return goodsMapper.selectPage(page, wrapper);
        } else {
            wrapper.eq("manufacturer_id", manufacturerId)
                    .like("goods_name", query)
                    .or().like("description", query)
                    .or().like("manufacturer_name", query)
                    .or().like("manufacturer_address", query);
            return goodsMapper.selectPage(page, wrapper);
        }
    }

    @Override
    public Goods selectByGoodsId(int goodsId) {
        return goodsMapper.selectById(goodsId);
    }

    @Override
    public void add(Goods goods) {
        Manufacturer manufacturer = manufacturerMapper.selectById(goods.getManufacturerId());
        goods.setManufacturerName(manufacturer.getManufacturerName());
        goods.setManufacturerAddress(manufacturer.getManufacturerAddress());
        goodsMapper.insert(goods);
    }

    @Override
    public void update(Goods goods) {
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.eq("goods_id", goods.getGoodsId());
        goodsMapper.update(goods, wrapper);
    }

    @Override
    public int delete(String goodsId) {
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.eq("goods_id", goodsId);
        return goodsMapper.delete(wrapper);
    }

    @Override
    public List<Goods> selectSomeByName(String name) {
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        wrapper.like("goods_name", name);
        return goodsMapper.selectList(wrapper);
    }
}
