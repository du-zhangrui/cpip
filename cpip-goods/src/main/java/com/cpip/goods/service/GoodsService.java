package com.cpip.goods.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cpip.common.pojo.Goods;

import java.util.List;

public interface GoodsService {
    Page<Goods> selectAll(int current, int pageSize, String query);

    void add(Goods goods);

    void update(Goods goods);

    int delete(String goodsId);

    List<Goods> selectSomeByName(String name);

    Page<Goods> selectAllByManufacturerId(int manufacturerId, int current, int pageSize, String query);

    Goods selectByGoodsId(int goodsId);
}
