package com.cpip.goods;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cpip.goods.mapper")
public class StarterGoods {
    public static void main(String[] args) {
        SpringApplication.run(StarterGoods.class, args);
    }
}
