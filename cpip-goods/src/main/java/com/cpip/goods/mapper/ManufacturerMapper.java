package com.cpip.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cpip.common.pojo.Manufacturer;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufacturerMapper extends BaseMapper<Manufacturer> {
}
